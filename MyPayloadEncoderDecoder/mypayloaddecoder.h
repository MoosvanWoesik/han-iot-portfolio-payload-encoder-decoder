/*--------------------------------------------------------------------
  This file is part of the han-iot-portfolio-payload-encoder-decoder.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
 * \file mypayloaddecoder.h
 * \brief payloaddecoder class
 * \author Remko Welling (remko.welling@han.nl)
 * \date 3-3-2020
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 3-3-2020   | Initial version
 * 0.2    | 14-9-2020  | replaced exisiting implementenation fro one with a buffer, added setters and getters.
 *
 *
 */

#ifndef MY_PAYLOAD_DECODER_H
#define MY_PAYLOAD_DECODER_H

#include <stdint.h> // uint8_t, uint16_t, and uint32_t type

const uint8_t VALUE_1 = 0;
const uint8_t VALUE_2 = 4;

/// \brief payload decode class
/// This class wil decode variables out of a payload for use in a LoRaWAN application.
/// The class is setup using both .h and .cpp files where the setters and getters are
/// placed in to the .h file.
class myPayloadDecoder
{
private:
    uint8_t *_buffer;           ///< buffer containing payload with sensor data
    uint8_t _bufferSize;        ///< Size of payload for housekeeping.

    uint32_t _demoVariable1;     ///< demo variable 1, 32 bits sized
    uint16_t _demoVariable2;     ///< demo variable 2, 16 bits sized

    /// \brief Get uint32_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint32_t extract_uint32(const uint8_t *buf, const unsigned char idx = 0);

    /// \brief Get uint16_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint16_t extract_uint16(const uint8_t *buf, const unsigned char idx = 0);

    /// \brief Get uint8_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint8_t extract_uint8 (const uint8_t *buf, const unsigned char idx = 0);

public:
    myPayloadDecoder();     ///< Constructor
    ~myPayloadDecoder();    ///< Destuctor

    /// \brief set pointer to buffer containing the payload
    /// \param *payload
    void setPayload(uint8_t *payload){_buffer = payload;}

    /// \brief set size of payload.
    /// \param size of payload in bytes.
    void setPayloadSize(uint8_t size){_bufferSize = size;}

    /// \brief decode payload and put individual values in member variables.
    /// \pre payload and size shall be set.
    void decodePayload();

    /// \brief "Getter" to demonstrate getting the value of a private member variable
    /// return The value of the variable
    int  getDemoVariable1() const {return _demoVariable1;}

    /// \brief "Getter" to demonstrate getting the value of a private member variable
    /// return The value of the variable
    int  getDemoVariable2() const {return _demoVariable2;}

};

#endif // MY_PAYLOAD_DECODER_H
