/*--------------------------------------------------------------------
  This file is part of the han-iot-portfolio-payload-encoder-decoder.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

#include <iostream>

#include "mypayloaddecoder.h"
#include "mypayloadencoder.h"
#include "test_myPayloadEncoderDecoder.h"

using namespace std;

void testGroup1()
{
    test1();
    test2();
}

/// \brief Test 1 and evaluation
void test1()
{
    // Test 1: known input 1
    myPayloadEncoder encoder;   /// Encoder object
    myPayloadDecoder decoder;   /// Decoder object

    int testVariable1 = 100;    /// Test variable containing value that shall be transferred
    int testVariable2 = 200;    /// Test variable containing value that shall be transferred

    encoder.setDemoVariable1(testVariable1);             // Set variable in encoder object.
    encoder.setDemoVariable2(testVariable2);             // Set variable in encoder object.
    encoder.composePayload();

    uint8_t *payloadBuffer = encoder.getPayload();
    uint8_t payloadSize = encoder.getPayloadSize();

    decoder.setPayload(payloadBuffer);
    decoder.setPayloadSize(payloadSize);
    decoder.decodePayload();

    int result1 = decoder.getDemoVariable1();            // Retrieve variable from decoder object and save in result1.
    int result2 = decoder.getDemoVariable2();            // Retrieve variable from decoder object and save in result2.

    // evaluate test result of test 1
    cout << "Test 1 result: ";
    if(result1 == testVariable1)    {
        cout << "PASS" << endl;
    }else{
        cout << "FAIL" << endl;
    }

    // evaluate test result of test 2
    cout << "Test 2 result: ";
    if(result2 == testVariable2){
        cout << "PASS" << endl;
    }else{
        cout << "FAIL" << endl;
    }
}

/// \brief Test 2 and evaluation
void test2()
{
    // Test 1: known input 1
    myPayloadEncoder encoder;   /// Encoder object
    myPayloadDecoder decoder;   /// Decoder object

    int testVariable1 = -1;    /// Test variable containing value that shall be transferred
    int testVariable2 = 65540;    /// Test variable containing value that shall be transferred

    encoder.setDemoVariable1(testVariable1);             // Set variable in encoder object.
    encoder.setDemoVariable2(testVariable2);             // Set variable in encoder object.
    encoder.composePayload();

    uint8_t *payloadBuffer = encoder.getPayload();
    uint8_t payloadSize = encoder.getPayloadSize();

    decoder.setPayload(payloadBuffer);
    decoder.setPayloadSize(payloadSize);
    decoder.decodePayload();

    int result1 = decoder.getDemoVariable1();            // Retrieve variable from decoder object and save in result1.
    int result2 = decoder.getDemoVariable2();            // Retrieve variable from decoder object and save in result2.

    // evaluate test result of test 1
    cout << "Test 1 result: ";
    if(result1 == testVariable1){
        cout << "PASS" << endl;
    }else{
        cout << "FAIL" << endl;
    }

    // evaluate test result of test 2
    cout << "Test 2 result: ";
    if(result2 == testVariable2){
        cout << "PASS" << endl;
    }else{
        cout << "FAIL" << endl;
    }
}



